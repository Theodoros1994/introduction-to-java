package data;
/**
 * 
 * @author Κανακακη_Βελης_Καλμανιδης
 * Η κλαση Grade εχει σαν μεταβλητες βαθμο, αριθμο μητρωου και κωδικο μαθηματος
 */

public class Grade {

    private String G_Student_AM;
    private String G_CourseID;
    private double Grade;

    // constructor ginete elexos me exception error wste h vathomologia na einai entos orion
    
    public Grade(String G_Student_AM, String G_CourseID, double Grade) throws Exception {
        this.G_Student_AM = G_Student_AM;
        this.G_CourseID = G_CourseID;
        if (Grade < 0 || Grade > 10.0) {
            throw new Exception("Ektos oriwn eisagogi bathomologias (0-10)!");
        }
        this.Grade = Grade;
    }

    public String getG_Student_AM() {
        return G_Student_AM;
    }

    public void setG_Student_AM(String G_Student_AM) {
        this.G_Student_AM = G_Student_AM;
    }

    public String getG_CourseID() {
        return G_CourseID;
    }

    public void setG_CourseID(String G_CourseID) {
        this.G_CourseID = G_CourseID;
    }

    public double getGrade() {
        return Grade;
    }

    public void setGrade(double Grade) {
        this.Grade = Grade;
    }


    /**
     * Μεθοδος η οποια επιστρεφει την βαθμολογια σε μαθημα του φοιτητη
     */
    public void ShowAll() {

        System.out.println("AM foititi : " + G_Student_AM);
        System.out.println("Mathima : " + G_CourseID);
        System.out.println(" Vathomologia : " + Grade);
    }

    /**
     * Μεθοδος η οποια επιστρεφει την εγγραφη φοιτιτη συνολικα ως string
     *
     * @return ενα string με ολα τα στοιχεια της βαθμολογιας
     */
    @Override
    public String toString() {

        String AllInOne;
        AllInOne = G_Student_AM + " / " + G_CourseID + " / " + Grade;
        return AllInOne;
    }
}
