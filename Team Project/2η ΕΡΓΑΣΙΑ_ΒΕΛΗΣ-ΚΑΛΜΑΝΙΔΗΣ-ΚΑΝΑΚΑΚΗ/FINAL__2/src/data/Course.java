package data;

// Η κλαση Course εχει σαν μεταβλητες κωδικο μαθηματος , ονομα μαθηματος και εξαμηνο

public class Course {

    private String CourseID;
    private String CourseName;
    private int CourseSemester;

    //Constructors
    public Course(String CourseID, String CourseName, int CourseSemester) {

        this.CourseID = CourseID;
        this.CourseName = CourseName;
        this.CourseSemester = CourseSemester;
    }

    public Course(String CourseName) {
        this.CourseName = CourseName;
    }

    // Getters and Setters
    public String getCourseID() {
        return CourseID;
    }

    public void setCourseID(String CourseID) {
        this.CourseID = CourseID;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String CourseName) {
        this.CourseName = CourseName;
    }

    public int getCourseSemester() {
        return CourseSemester;
    }

    public void setCourseSemester(int CourseSemester) {
        this.CourseSemester = CourseSemester;
    }

    /**
     * Μεθοδος η οποια επιστρεφει την εγγραφη μαθηματος
     */
    public void ShowAll() {
        System.out.println("Stoixeia Mathimatos");
        System.out.println("=====================");
        System.out.println("Kwdikos: " + CourseID);
        System.out.println("Titlos : " + CourseName);
        System.out.println("Eksamino: " + CourseSemester);
    }


    /**
     * Μεθοδος η οποια επιστρεφει την εγγραφη συνολικα ως string
     * @return ενα string με ολα τα στοιχεια του μαθηματος
     */
    @Override
    public String toString() {

        String AllInOne;
        AllInOne = CourseID + " / " + CourseName + " / " + CourseSemester;
        return AllInOne;
    }
}

