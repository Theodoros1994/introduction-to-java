package data;

import java.util.ArrayList;
// Η κλαση Person κληρονομειται απο τις κλασεις Professor και Student

public class Person {
    protected String SurName;
    protected String FirstName;
    protected String Email;
    protected String Phone;
    protected ArrayList<Course> Courses;

    // Constructor
    public Person(String SurName, String FirstName, String Email, String Phone) {

        this.SurName = SurName;
        this.FirstName = FirstName;
        this.Email = Email;
        this.Phone = Phone;
        Courses = new ArrayList();
    }

    // Getters and Setters

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String SurName) {
        this.SurName = SurName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public ArrayList<Course> getCourses() {
        return Courses;
    }

    public void setCourses(ArrayList<Course> Courses) {
        this.Courses = Courses;
    }

    /**
     * Προσθετει ενα μαθημα στη λιστα των μαθηματων του ατομου
     *
     * @param c Το μαθημα που θα προστεθει
     */
    public void AddCourse(Course c) {
        Courses.add(c);
    }

    /**
     * Διαγραφει ενα μαθημα απο τη λιστα των μαθηματων ενος ατομου. Σε περιπτωση που το μαθημα δεν βρισκεται στη λιστα δεεν
     * πραγματοποιειται καποια ενεργεια. Μπορει να υλοποιηθει και με πιο απλους τροπους...
     *
     * @param CourseID Ο κωδικος η το λεκτικο του μαθηματος που θα αφαιρεθει απο τη λιστα
     */
    public void RemoveCourse(String CourseID) {

        for (int i = 0; i < Courses.size(); i++) {
            if (Courses.get(i).getCourseID().equals(CourseID) || Courses.get(i).getCourseName().equals(CourseID)) {
                Courses.remove(i);

            }
        }
    }

    /**
     * Ελεγχει αν το ατομο ασχολειται με ενα συγκεκριμενο μαθημα. Ο ελεγχος γινεται με βαση τον κωδικο του μαθηματος
     * η το λεκτικο του. Η αναζητηση πραγματοποιειται με δομη επαναληψης for each
     *
     * @param CourseID o κωδικος η το λεκτικο του προς αναζητηση μαθηματος
     * @return true αν το μαθημα βρεθηκε. false διαφορετικα
     */
    public Boolean HasCourse(String CourseID) {

        for (Course c : Courses) {

            if (c.getCourseID().equals(CourseID) || c.getCourseName().equals(CourseID))
                return true;
        }
        return false;
    }


    /**
     * Μεθοδος η οποια επιστρεφει ως string τη λιστα με τα μαθηματα του ατομου και συγκεκριμενα μονο την ονομασια τους
     * Σε περιπτωση που δεν εχει ανατεθει μαθημα στο ατομο επιστρεφει αντιστοιχο μηνυμα
     *
     * @return Res String το οποιο  περιεχει τα μαθηματα χωρισμενα με ", "
     */
    public String GetCoursesNames() {
        StringBuilder Res = new StringBuilder();
        if (this.Courses == null) {
            Res = new StringBuilder("Den exei anatethei mathima");
        } else {

            for (int i = 0; i < this.Courses.size(); i++) {

                Course tmp = this.Courses.get(i);
                Res.append(tmp.getCourseName());
                if (i < (this.Courses.size() - 1))
                    Res.append(", ");
            }
        }
        return Res.toString();
    }

    /**
     * Μεθοδος η οποια επιστρεφει την εγγραφη του ατομου
     */
    public void ShowAll() {

        System.out.println("ΣΤΟΙΧΕΙΑ ΑΤΟΜΟΥ ");
        System.out.println("================ ");
        System.out.println("Επωνυμο........: " + SurName);
        System.out.println("Επωνυμο........: " + FirstName);
        System.out.println("Email........: " + Email);
        System.out.println("Τηλεφωνο........: " + Phone);
        //        System.out.println("ΣΤΟΙΧΕΙΑ ΑΤΟΜΟΥ ");


    }

    /**
     * Μεθοδος η οποια επιστρεφει την εγγραφη συνολικα ως String
     *
     * @return ενα string  με ολα τα στοιχεια του ατομου
     */
    @Override
    public String toString() {
        String AllInOne;

        AllInOne = SurName + " / " + FirstName + " / " + Email + " / " + Phone;
        return AllInOne;

    }
}