package data;

import entities.Professor;
import entities.Student;
import java.io.*;
import java.util.ArrayList;
/**
 * 
 * @author Κανακακη_Βελης_Καλμανιδης
 * Η κλαση Lists παριστανει τις λιστες καθηγητων-φοιτητων-μαθηματων-βαθμολογιων
 */
public class Lists {

    public ArrayList<Person> P_list;
    public ArrayList<Course> C_list;
    public ArrayList<Grade> Grades;

    private final String SaveDir = "C:\\Users\\nana\\Desktop\\";

    public Lists() {
        P_list = new ArrayList();
        C_list = new ArrayList();
        Grades = new ArrayList();
    }

    // Getters
    public ArrayList<Person> getP_list() {
        return P_list;
    }

    public ArrayList<Course> getC_list() {
        return C_list;
    }

    public ArrayList<Grade> getGrades() {
        return Grades;
    }

    /**
     * Μεθοδος εισαγωγης νεας εγγραφης ατομου στη λιστα
     *
     * @param p Το ατομο που προκειται να προστεθει
     */
    public void AddPerson(Person p) {
        P_list.add(p);
    }

    /**
     * Μεθοδος εισαγωγης νεας εγγραφης μαθηματος στη λιστα
     *
     * @param c Το μαθημα που προκειται να προστεθει
     */

    public void AddCourse(Course c) {
        C_list.add(c);
    }

    /**
     * Μεθοδος εισαγωγης νεας βαθμολογιας μαθηματος στη λιστα
     *
     * @param g Ο βαθμος που προκειται να προστεθει
     */
    public void AddGrade(Grade g) {
        Grades.add(g);
    }

    /**
     * Μεθοδος διαγραφης ατομου απο τη λιστα ατομων
     *
     * @param p το προς αναζητηση ατομο
     */

    public void DelPerson(Person p) {
        P_list.remove(p);
    }


    /**
     * Μεθοδος διαγραφης μαθηματος απο τη λιστα μαθηματων
     *
     * @param c Το προς αναζητηση ατομο
     */

    public void DelCourse(Course c) {
        C_list.remove(c);
    }

    /**
     * Μεθοδος ελεγχου υπαρξης ατομου στη λιστα των ατομων
     *
     * @param p το προς αναζητηση ατομο
     * @return true αν βρεθει false αλλιως
     */
    public Boolean CheckPerson(Person p) {
        for (Person tmp : P_list) {
            if (tmp == p)
                return true;
        }
        return false;
    }



    /**
     * Μεθοδος ελεγχου υπαρξης καθηγητη στη λιστα των ατομων
     *
     * @param ProfCode ο κωδικος καθηγητη προς αναζητηση
     * @return true αν βρεθει false αν οχι
     */
    public boolean CheckProf(String ProfCode) {

        for (Person tmp : P_list) {
            if (tmp instanceof Professor && ((Professor) tmp).getProfCode().equals(ProfCode))
                return true;
        }
        return false;
    }


    /**
     * Μεθοδος ελεγχου υπαρξης φοιτητη στη λιστα των ατομων
     *
     * @param AM ο αριθμος μητρωου του φοιτητη προς αναζητηση
     * @return true αν βρεθει false αν οχι
     */
    public boolean CheckStudent(String AM) {

        for (Person tmp : P_list) {
            if (tmp instanceof Student && ((Student) tmp).getAM().equals(AM))
                return true;
        }
        return false;
    }

    /**
     * Μεθοδος ελεγχου υπαρξης μαθηματος στη λιστα των μαθηματων
     *
     * @param CourseID ο κωδικος η το λεκτικο του μαθηματος που θα αφαιρεθει απο τη λιστα
     * @return true αν βρεθει false αν οχι
     */
    public boolean CheckCourse(String CourseID) {

        for (Course tmp : C_list) {
            if (tmp.getCourseID().equals(CourseID) || tmp.getCourseName().equals(CourseID))
                return true;
        }
        return false;
    }

    /**
     * Μεθοδος ελεγχου υπαρξης βαθμολογιας μαθηματος
     *
     * @param CourseID ο κωδικος του μαθηματος προς αναζητηση
     * @return true αν βρεθει false αν οχι
     */
    public boolean CheckGrade(String CourseID) {

        for (Grade tmp : Grades) {
            if (tmp.getG_CourseID().equals(CourseID))
                return true;
        }
        return false;
    }


    /**
     * Μεθοδος αναζητησης ατομου στη λιστα των ατομων
     *
     * @param SureName  το επιθετο προς αναζητηση ατομου
     * @param FirstName το ονομα του προς αναζητηση ατομου
     * @return tmp το ατομο σε περιπτωση που βρεθει h null
     */
    public Person FindPerson(String SureName, String FirstName) {

        for (Person tmp : P_list) {

            if (tmp.getSurName().equals(SureName) && tmp.getFirstName().equals(FirstName))
                return tmp;
        }
        System.out.println("Den yparxei tetoio atomo sti lista");
        return null;
    }

    /**
     * Μεθοδος αναζητησης βαθμολογιας μαθηματος
     *
     * @param CourseID ο κωδικος του μαθηματος προς αναζητηση
     * @return tmp τη βαθμολογια σε περιπτωση που βρεθει η null
     */
    public Grade FindGrade(String CourseID) {

        for (Grade tmp : Grades) {
            if (tmp.getG_CourseID().equals(CourseID))
                return tmp;
        }
        System.out.println("Den yparxei tetoia vathmologia");
        return null;
    }

    /**
     * Μεθοδος αναζητησης μαθηματος στη λιστα των μαθηματων
     *
     * @param CourseID ο κωδικος η το λεκτικο του μαθηματος που θα αφαιρεθει απο τη λιστα
     * @return tmp το μαθημα σε περιπτωση που βρεθει η null
     */
    public Course GetCourseByID(String CourseID) {

        for (Course tmp : C_list) {
            if (tmp.getCourseID().equals(CourseID) || tmp.getCourseName().equals(CourseID))
                return tmp;
        }
        return null;
    }

    /**
     * Μεθοδος υπολογισμου μεσου ορου βαθμολογια φοιτιτη
     *
     * @param AM ο αριθμος μητρωου του φοιτητη προς αναζητηση
     * @return to avg αποτελεσμα του μεσου ορου βαθμολογιας φοιτητη
     */
    public double AVG_Student(String AM) {

        int i = 0;
        double sum = 0;
        double avg;

        for (Grade tmp : Grades) {

            if (tmp.getG_Student_AM().equals(AM)) {

                i++;
                sum += tmp.getGrade();
            }
        }
        avg = sum / i;
        return avg;
    }


    /**
     * Μεθοδος υπολογισμου μεσου ορου βαθμολογιας μαθηματος
     *
     * @param CourseID ο κωδικος μαθηματος προς αναζητηση
     * @return avg το αποτελεσμα του μεσου ορου βαθμολογιας μαθηματος
     */
    public double AVG_Course(String CourseID) {

        int i = 0;
        double sum = 0;
        double avg;

        for (Grade tmp : Grades) {
            if (tmp.getG_CourseID().equals(CourseID)) {
                i++;
                sum += tmp.getGrade();
            }
        }
        avg = sum / i;
        return avg;
    }


    /**
     * Μεθοδος αποθηκευσης της λιστας των καθηγητων σε αρχειο
     *
     * @param FN το ονομα με το οποιο θα αποθηκευτει το αρχειο
     * @return true αν αποθηκευτει false an oxi
     */

    public boolean SaveProfessors(String FN) {
        try {
            PrintWriter PW = new PrintWriter(SaveDir + FN + ".csv");
            for (Person tmp : P_list) 
            {

                if (tmp instanceof Professor) 
                {
                    Professor P = (Professor) tmp;
                    StringBuilder SB = new StringBuilder ();
                    SB.append(P.getProfCode())
                            .append(";")
                            .append(P.SurName)
                            .append(";")
                            .append(P.FirstName)
                            .append(";")
                            .append(P.Email)
                            .append(";")
                            .append(P.Phone)
                            .append(";")
                            .append(P.getExpertise())
                            .append(";")
                            .append(P.GetCoursesNames());
                    
                    String Tbr = SB.toString();
                    PW.println(Tbr);
                }
            }
            PW.close();
        } catch (FileNotFoundException e) {
            System.out.println("^^^" + e.getMessage() + "^^^");
            return false;
        }
        return true;
    }

    /**
     * Μεθοδος αποθηκευσης της λιστας των φοιτητων σε αρχειο
     *
     * @return true αν αποθηκευτει false αν οχι
     * @param FN το ονομα με το οποιο θα αποθηκευτει το αρχειο
     */
    public boolean SaveStudents(String FN) {

        try {
            PrintWriter PW = new PrintWriter(SaveDir + FN + ".csv");
            for (Person tmp : P_list) {

                if (tmp instanceof Student) {
                    Student S = (Student) tmp;
                    StringBuilder SB = new StringBuilder();
                    SB.append(S.getAM())
                            .append(";")
                            .append(S.SurName)
                            .append(";")
                            .append(S.FirstName)
                            .append(";")
                            .append(S.Email)
                            .append(";")
                            .append(S.Phone)
                            .append(";")
                            .append(S.getSemester())
                            .append(";")
                            .append(S.GetCoursesNames());

                    String Tbr = SB.toString();
                    PW.println(Tbr);
                }
            }
            PW.close();
        } catch (FileNotFoundException e) {
            System.out.println("^^^" + e.getMessage() + "^^^");
            return false;
        }
        return true;
    }

    /**
     * Μεθοδος αποθηκευσης της λιστας των μαθηματων σε αρχειο
     *
     * @param FN το ονομα με το οποιο θα αποθηκευτει το αρχειο
     * @return true αν αποθηκευτει false αν οχι
     */
    public boolean SaveCourses(String FN) {

        try {
            PrintWriter PW = new PrintWriter(SaveDir + FN + ".csv");
            for (Course tmp : C_list) {
//if (tmp instanceof Student){
//Student S = (Student) tmp;
                StringBuilder SB = new StringBuilder();
                SB.append(tmp.getCourseID())
                        .append(";")
                        .append(tmp.getCourseName())
                        .append(";")
                        .append(tmp.getCourseSemester());
                String Tbr = SB.toString();
                PW.println(Tbr);
            }
            PW.close();
        } catch (FileNotFoundException e) {
            System.out.println("^^^" + e.getMessage() + "^^^");
            return false;
        }
        return true;
    }


    /**
     * Μεθοδος αποθηκευσης της λιστας των βαθμολογιων σε αρχειο
     *
     * @param FN το ονομα με το οποιο θα αποθηκευτει το αρχειο
     * @return true αν βρεθει αλλιως false
     */
    public boolean SaveGrades(String FN) {

        try {
            PrintWriter PW = new PrintWriter(SaveDir + FN + ".csv");
            for (Grade tmp : Grades) {
                //if (tmp instanceof Student){
                //Student S = (Student) tmp;
                StringBuilder SB = new StringBuilder();
                SB.append(tmp.getG_Student_AM())
                        .append(";")
                        .append(tmp.getG_CourseID())
                        .append(";")
                        .append(tmp.getGrade());

                String Tbr = SB.toString();
                PW.println(Tbr);
            }
            PW.close();
        } catch (FileNotFoundException e) {
            System.out.println("^^^" + e.getMessage() + "^^^");
            return false;
        }
        return true;
    }

    /**
     * Μςθοδος φορτωσης της λιστας των καθηγητων απο το αρχειο
     *
     * @param FN το ονομα του προς ανακτηση αρχειου
     * @return true αν βρεθει αλλιως false
     */
    public boolean LoadProfessors(String FN) {

        boolean Ok = false;

        //Κωδικας εκκαθαρισης καρφωτων εγγραφων
        ArrayList<Person> TmpList = new ArrayList();
        for (Person tmp : P_list) {
            if (tmp instanceof Professor) {
                TmpList.add(tmp);
            }
        }
        P_list.removeAll(TmpList);
        TmpList = null;
        //Τελος κωδικα εκαθαρισης καρφωτων εγγραφων

        try {
            FileInputStream fs = new FileInputStream(SaveDir + FN + ".csv");
            BufferedReader br = new BufferedReader(new InputStreamReader(fs));
            String StrLine;
            while ((StrLine = br.readLine()) != null) {

                String[] Parts = StrLine.split(";");
                Professor Pr = new Professor(Parts[0], Parts[1], Parts[2], Parts[3], Parts[4], Parts[5]);

                P_list.add(Pr);

                if (Parts.length > 6) {
                    ArrayList<Course> tmpCList = new ArrayList();
                    String[] tmpCourse = Parts[6].split(",");

                    for (int i = 0; i < tmpCourse.length; i++) {

                        Course tmpC = new Course(null);
                        tmpC.setCourseName(tmpCourse[i]);
                        for (int j = 0; j < C_list.size(); j++) {
                            if (tmpC.getCourseName().equals(C_list.get(j).getCourseName())) {
                                tmpCList.add(C_list.get(j));
                            }
                        }
                    }
                    Pr.setCourses(tmpCList);
                } else {
                    Pr.setCourses(null);
                }
            }
            fs.close();
            Ok = true;
        } catch (FileNotFoundException e) {
            System.out.println("Den mporei na diabasei to arxeio " + SaveDir + FN);
        } catch (IOException e) {
            e.printStackTrace();// Τυπωνεις τις εξαιρεσεις μαζι με πληροφοριες οπως τη γραμμη και την κλαση
        }
        return Ok;
    }

    /**
     * Μεθοδος φορτωσης της λιστας των φοιτητων απο το αρχειο
     *
     * @param FN το ονομα του προς ανακτηση αρχειου
     * @return true αν φορτωθει false αν οχι
     */
    public boolean LoadStudents(String FN) {

        boolean Ok = false;

        //Κωδικας εκκαθαρισης καρφωτων εγγραφων
        ArrayList<Person> TmpList = new ArrayList();
        for (Person tmp : P_list) {
            if (tmp instanceof Student) {
                TmpList.add(tmp);
            }
        }
        P_list.removeAll(TmpList);
        TmpList = null;
        //Τελος κωδικα εκαθαρισης καρφωτων εγγραφων

        try {

            FileInputStream fs = new FileInputStream(SaveDir + FN + ".csv");
            BufferedReader br = new BufferedReader(new InputStreamReader(fs));
            String StrLine;
            while ((StrLine = br.readLine()) != null) {

                String[] Parts = StrLine.split(";");
                Student St = new Student(Parts[0], Parts[1], Parts[2], Parts[3], Parts[4], Integer.parseInt(Parts[5]));

                P_list.add(St);

                if (Parts.length > 6) {
                    ArrayList<Course> tmpCList = new ArrayList();
                    String[] tmpCourse = Parts[6].split(",");

                    for (int i = 0; i < tmpCourse.length; i++) {

                        Course tmpC = new Course(null);
                        tmpC.setCourseName(tmpCourse[i]);
                        for (int j = 0; j < C_list.size(); j++) {
                            if (tmpC.getCourseName().equals(C_list.get(j).getCourseName())) {
                                tmpCList.add(C_list.get(j));
                            }
                        }
                    }
                    St.setCourses(tmpCList);

                } else {
                    St.setCourses(null);
                }
            }
            fs.close();
            Ok = true;
        } catch (FileNotFoundException e) {
            System.out.println("Den mporei na diabasei to arxeio " + SaveDir + FN);
        } catch (IOException e) {
            e.printStackTrace();// Τυπωνεις τις εξαιρεσεις μαζι με πληροφοριες οπως τη γραμμη και την κλαση
        }
        return Ok;
    }


    /**
     * Μεθοδος φορτωσης λιστας μαθηματων απο το αρχειο
     *
     * @param FN το ονομα του προς ανακτηση αρχειου
     * @return true αν φορτωθει false αν οχι
     */
    public boolean LoadCourses(String FN) {

        boolean Ok = false;

        //Κωδικας εκκαθαρισης καρφωτων εγγραφων
        ArrayList<Course> TmpList = new ArrayList();
        for (Course tmp : C_list) {

            TmpList.add(tmp);
        }
        C_list.removeAll(TmpList);
        TmpList = null;
        //Τελος κωδικα εκαθαρισης καρφωτων εγγραφων

        try {
            FileInputStream fs = new FileInputStream(SaveDir + FN + ".csv");
            BufferedReader br = new BufferedReader(new InputStreamReader(fs));
            String StrLine;
            while ((StrLine = br.readLine()) != null) {

                String[] Parts = StrLine.split(";");
                Course Co = new Course(Parts[0], Parts[1], Integer.parseInt(Parts[2]));

                C_list.add(Co);

            }
            fs.close();
            Ok = true;
        } catch (FileNotFoundException e) {
            System.out.println("Den mporei na diabasei to arxeio " + SaveDir + FN);
        } catch (IOException e) {
            e.printStackTrace();// Τυπωνεις τις εξαιρεσεις μαζι με πληροφοριες οπως τη γραμμη και την κλαση
        }
        return Ok;
    }

    /**
     * Μεθοδος φορτωσης τηε λιστας των βαθμολογιων απο αρχειο
     *
     * @param FN το ονομα του προς ανακτησης αρχειου
     * @return true αν φορτωθει false αν οχι
     */
    public boolean LoadGrades(String FN) {

        boolean Ok = false;

        //Κωδικας εκκαθαρισης καρφωτων εγγραφων
        ArrayList<Grade> TmpList = new ArrayList();
        for (Grade tmp : Grades) {

            TmpList.add(tmp);
        }
        Grades.removeAll(TmpList);
        TmpList = null;
        //Τελος κωδικα εκαθαρισης καρφωτων εγγραφων

        try {
            FileInputStream fs = new FileInputStream(SaveDir + FN + ".csv");
            BufferedReader br = new BufferedReader(new InputStreamReader(fs));
            String StrLine;
            while ((StrLine = br.readLine()) != null) {

                String[] Parts = StrLine.split(";");
                Grade Gr = new Grade(Parts[0], Parts[1], Double.parseDouble(Parts[2]));

                Grades.add(Gr);
            }
            fs.close();
            Ok = true;
        } catch (FileNotFoundException e) {
            System.out.println("Den mporei na diabasei to arxeio " + SaveDir + FN);
        } catch (IOException e) {
            e.printStackTrace();// Τυπωνεις τις εξαιρεσεις μαζι με πληροφοριες οπως τη γραμμη και την κλαση
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return Ok;
    }
}
