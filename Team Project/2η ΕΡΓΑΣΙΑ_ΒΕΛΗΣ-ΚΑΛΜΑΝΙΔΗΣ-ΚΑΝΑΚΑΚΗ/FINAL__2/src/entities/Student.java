package entities;


import data.Person;

/**
 * 
 * @author Κανακακη_Βελης_Καλμανιδης
 * H κλαση Student ειναι απογονος της κλασης Person
 */

public class Student extends Person {

    private String AM;
    private int Semester;


    // Constructor
    public Student(String AM, String SurName, String FirstName, String Email, String Phone, int Semester) {

        super(SurName, FirstName, Email, Phone);

        this.AM = AM;
        this.Semester = Semester;
    }
    // Getters Setters

    public String getAM() {
        return AM;
    }

    public void setAM(String AM) {
        this.AM = AM;
    }

    public int getSemester() {
        return Semester;
    }

    public void setSemester(int Semester) {
        this.Semester = Semester;
    }

    /**
     * Μεθοδος η οποια επιστρεφει την εγγραφη φοιτητη
     */

    @Override
    public void ShowAll() {
        super.ShowAll();
        System.out.println("ΑΜ " + AM);
        System.out.println("Eksamino" + Semester);
    }

    @Override
    /**
     * Μεθοδος η οποια επιστρεφει την εγγραφη φοιτητη συνολικα ως string
     * @return ενα string με ολα τα στοιχεια του φοιτητη
     */
    public String toString() {

        String AllInOne;
        AllInOne = AM + " / " + super.toString() + " / " + Semester;
        return AllInOne;
    }
}
