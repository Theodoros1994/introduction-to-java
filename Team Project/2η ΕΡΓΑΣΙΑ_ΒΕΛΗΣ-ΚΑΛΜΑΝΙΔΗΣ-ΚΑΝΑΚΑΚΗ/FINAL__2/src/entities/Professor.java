package entities;

import data.Person;

/**
 *
 * @author Κανακακη_Βελης_Καλμανιδης
 * H κλαση Professor ειναι απογονος της κλασης Person
 */
public class Professor extends Person {

    private String ProfCode;
    private String Expertise;

    // constructor

    /**
     *
     * @param ProfCode κωδικος καθηγητη
     * @param SurName επιθετο καθηγητη
     * @param FirstName ονομα καθηγητη
     * @param Email μειλ καθηγητη
     * @param Phone τηλεφωνο καθηγητη
     * @param Expertise μαθημα καθηγητη
     */
    public Professor(String ProfCode, String SurName, String FirstName, String Email, String Phone, String Expertise) {

        super(SurName, FirstName, Email, Phone);
        this.ProfCode = ProfCode;
        this.Expertise = Expertise;

    }
    // Getters and Setters

    /**
     *
     * @return
     */
    public String getProfCode() {
        return ProfCode;
    }

    /**
     *
     * @param ProfCode
     */
    public void setProfCode(String ProfCode) {
        this.ProfCode = ProfCode;
    }

    /**
     *
     * @return
     */
    public String getExpertise() {
        return Expertise;
    }

    /**
     *
     * @param Expertise
     */
    public void setExpertise(String Expertise) {
        this.Expertise = Expertise;
    }


    /**
     * Μεθοδος η οποια επιστρεφει την εγγραφη καθηγητη
     */
    @Override
    public void ShowAll() {
        super.ShowAll();
        System.out.println("Κωδικος " + ProfCode);
        System.out.println("Ειδικοτητα" + Expertise);

    }


    /**
     * Μεθοδος η οποια επιστρεφει την εγγραφη καθηγητη συνολικα ως string
     *@return ενα string με ολα τα στοιχεια του καθηγητη

     */
    @Override
    public String toString() {

        String AllInOne;
        AllInOne = ProfCode + " / " + super.toString() + " / " + Expertise;
        return AllInOne;
    }
}
