package ui;

import data.Course;
import data.Grade;
import data.Lists;
import data.Person;
import entities.Professor;
import entities.Student;
import java.util.Scanner;

/**
 *
 * @author Κανακακη_Βελης_Καλμανιδης
 * Η βασικη κλαση FoititologioMenu ενσωματωνει το user interface με ολες τις διαθεσιμες επιλογες
 */
public class FoititologioMenu {

    private Lists MainList;
    private Scanner input;

    /**
     * Μεθοδος εισαγωγης στοιχειων απο πληκτρολογιο
     */
    public FoititologioMenu() {

        MainList = new Lists();
        FixedRecords();
        input = new Scanner(System.in, "Windows-1253");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("********************");

        System.out.println("Εφαρμογή πληκτρολογίου");
        FoititologioMenu Final = new FoititologioMenu();
        Final.Menu();
    }

    /**
     * Το μενου της εφαρμογης 
     * Ελεγχοι που πραγματοποιουνται:
     * ελεγχος για εισαγωγη αριθμου εκτος επιλογων μενου
     * ελεγχος για εισαγωγη οποιουδηποτε αλλου συνδυασμου ψηφιων εκτος αριθμου (int)
     */
    private void Menu() {

        int menu;

        do {

            System.out.println("=======================");
            System.out.println(" 1. Προβολή καθηγητών  ");
            System.out.println(" 2. Προβολή φοιτητών  ");
            System.out.println(" 3. Προβολή μαθημάτων  ");
            System.out.println(" 4. Προβολή βαθμολογιών ");
            System.out.println(" 5. Προσθήκη ατόμου ");
            System.out.println(" 6. Διόρθωση στοιχείων ατόμου ");
            System.out.println(" 7. Διαγραφή ατόμου ");
            System.out.println(" 8. Προσθήκη νέου μαθήματος ");
            System.out.println(" 9. Διόρθωση στοιχείων μαθήματος  ");
            System.out.println(" 10. Διαγραφή μαθήματος ");
            System.out.println(" 11. Ανάθεση μαθήματος σε άτομο ");
            System.out.println(" 12. Διαγραφή μαθήματος από άτομο ");
            System.out.println(" 13. Καταχώρηση βαθμολογίας σε φοιτητή ");
            System.out.println(" 14. Εμφάνιση στατιστικών");
            System.out.println(" 15. Αποθήκευση/Φόρτωση δεδομένων ");
            System.out.println(" 0. Έξοδος ");
            System.out.println(" =========================  ");
            System.out.println(" Πληκτρολογήστε μία επιλογή:  ");
            System.out.println("   ");
            do {
                while (!input.hasNextInt()) {
                    System.out.println("Δεν επιλέξατε αριθμό.");
                    System.out.printf("Δώστε αποδεκτή τιμή μενού.");
                    input.next();

                }
                menu = input.nextInt();
            } while (menu < 0);
            System.out.println("");

            switch (menu) {

                case 1:
                    ShowProfessors();
                    Pause();
                    break;
                case 2:
                    ShowStudents();
                    Pause();
                    break;
                case 3:
                    ShowCourses();
                    Pause();
                    break;
                case 4:
                    ShowGrades();
                    Pause();
                    break;
                case 5:
                    AddPerson();
                    Pause();
                    break;
                case 6:
                    ManipulatePerson();
                    Pause();
                    break;
                case 7:
                    DeletePerson();
                    Pause();
                    break;
                case 8:
                    AddNewCourse();
                    Pause();
                    break;
                case 9:
                    ManipulateCourse();
                    Pause();
                    break;
                case 10:
                    DeleteCourse();
                    Pause();
                    break;
                case 11:
                    AddCourseToPerson();
                    Pause();
                    break;
                case 12:
                    DeleteCourseFromPerson();
                    Pause();
                    break;
                case 13:
                    AddGrades();
                    Pause();
                    break;
                case 14:
                    AvgOfAllStudents();
                    AvgOfAllCourses();
                    Pause();
                    break;
                case 15:
                    LoadSaveDialogue();
                    Pause();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Δεν υπάρχει αυτή η επιλογή.");
                    Pause();
                    System.out.println("");
                    break;
            }
        } while (menu != 0);
        System.out.println("Επιλέχθηκε έξοδος από την εφαρμογή.");
    }

    // Μεθοδος παυσης εκτελεσης προγραμματος μεχρι να πατηθει enter
    private void Pause() {
        Scanner K = new Scanner(System.in);
        System.out.println("");
        System.out.printf("Πιέστε enter για να συνεχίσετε...");
        K.nextLine();
        System.out.println("");
    }

    /**
     * Βοηθητικη μεθοδος για λογους εξετασης λειτουργιας του προγραμματος
     * Προσθετει τιμες εγγραφων σε καθε λιστα
     * Γινεται ελεγχος με exceptions ωστε η βαθμολογια να ειναι εντος οριων
     */


    private void FixedRecords() {
        Course c;

        c = new Course("001", "Pliroforiki", 3);
        MainList.AddCourse(c);
        c = new Course("002", "Asfaleia Diktuwn", 1);
        MainList.AddCourse(c);
        c = new Course("003", "Baseis Dedomenon", 2);
        MainList.AddCourse(c);
        
        Student s;
        s = new Student("F001", "Stamatiou", "Stamatis", "stam@stam.com", "6987452314", 3);
        MainList.AddPerson(s);
        s.AddCourse(MainList.GetCourseByID("001"));
        s.AddCourse(MainList.GetCourseByID("003"));
        s = new Student("F002", "Petrou", "Petros", "petr@petr.com", "6939088901", 1);
        MainList.AddPerson(s);
        s = new Student("F003", "Ioannou", "Ioannis", "ioan@ioan.com", "6940231256", 1);
        MainList.AddPerson(s);

        try {
            Grade g1 = new Grade("F001", "001", 5.2);
            MainList.AddGrade(g1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        try {
            Grade g2 = new Grade("F001", "003", 3.2);
            MainList.AddGrade(g2);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            Grade g3 = new Grade("F002", "002", 6.2);
            MainList.AddGrade(g3);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            Grade g4 = new Grade("F003", "003", 4.2);
            MainList.AddGrade(g4);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        Professor p;
        p = new Professor("K005", "Nikolaou", "Nikolaos", "nik@nik.com", "2115959312", "Pliroforiki");
        MainList.AddPerson(p);
        p.AddCourse(MainList.GetCourseByID("001"));
        p.AddCourse(MainList.GetCourseByID("003"));
        p = new Professor("K008", "Konstantinou", "Konstantinos", "kon@kon.com", "2105720234", "diktya");
        MainList.AddPerson(p);
        p.AddCourse(MainList.GetCourseByID("002"));
        p = new Professor("K010", "Georgiou", "Georgios", "geo@geo.com", "2143209654", "vaseis dedomenon");
        MainList.AddPerson(p);
        p.AddCourse(MainList.GetCourseByID("003"));
    }

    /**
     * Μεθοδος εμφανισης της λιστας με τους καθηγητες
     * Επιστρεφει τη λιστα με τους καθηγητες της κλασης @Person καθως και την ονομασια των μαθηματων τα οποια διδασκουν με χρηση της @GetCour
     */

    private void ShowProfessors() {

        int i = 1;
        System.out.println("Λίστα καθηγητών");
        System.out.println("===============");
        System.out.println(" A/A Kωδ/Kαθ     Eπώνυμο              Όνομα                 Email              Tηλέφωνο      Eιδικότητα             Mαθήματα ");
        System.out.println("============================================================================================================================");

        for (Person tmp : MainList.getP_list()) {

            if (tmp instanceof Professor)
                System.out.printf("%3d  %-10s %-20s %-20s %-20s %-12s %-20s   Διδάσκει: %s\n", i++,
                        ((Professor) tmp).getProfCode(), tmp.getSurName(), tmp.getFirstName(), tmp.getEmail(),
                        tmp.getPhone(), ((Professor) tmp).getExpertise(), tmp.GetCoursesNames());
        }
    }

    private void ShowStudents() {

        int i = 1;
        System.out.println("Λίστα Φοιτητών");
        System.out.println("===============");
        System.out.println(" A/A A/M         Eπώνυμο                Όνομα                    Email              Tηλέφωνο     Eξάμηνο           Mαθήματα ");
        System.out.println("=================");

        for (Person tmp : MainList.getP_list()) {

            if (tmp instanceof Student)
                System.out.printf("%2d  %-10s   %-20s  %-20s   %-20s  %-12s    %-10d   Παρακολουθεί:%s\n", i++,
                        ((Student) tmp).getAM(), tmp.getSurName(), tmp.getFirstName(), tmp.getEmail(),
                        tmp.getPhone(), ((Student) tmp).getSemester(), tmp.GetCoursesNames());
        }
    }

    /**
     * Μεθοδος εμφανισης της λιστας με τα μαθηματα του προγραμματος σπουδων
     */
    private void ShowGrades() {

        int i = 1;
        System.out.println("Λίστα βαθμολογιών");
        System.out.println("=================");
        System.out.println(" A/A   Μάθημα   Φοιτητής   Βαθμολογία   ");
        System.out.println("========================================");

        for (Grade tmp : MainList.getGrades()) {
            System.out.printf("%2d %5s %10s %15f\n",
                    i++, tmp.getG_CourseID(), tmp.getG_Student_AM(), tmp.getGrade());
        }
    }

    /**
     * Μεθοδος εμφανισης της λιστας με τις βαθμολογιες που εχουν λαβει οι φοιτητες
     */
    private void ShowCourses() {

        int i = 1;
        System.out.println("Λίστα μαθημάτων");
        System.out.println("===============");
        System.out.println(" A/A        Κωδικός Μαθήματος        Όνομα Μαθήματος            Εξάμηνο   ");
        System.out.println("=================");

        for (Course tmp : MainList.getC_list()) {
            System.out.printf("%-15d  %-18s   %-25s   %d\n", i++, tmp.getCourseID(), tmp.getCourseName(), tmp.getCourseSemester());
        }
    }


    /**
     * Μεθοδος προσθεσης νεου ατομου στη λιστα των ατομων
     * Δινεται στον χρηστη η δυνατοτητα της επιλογης προσθηκης καθηγητη ή φοιτητη
     * Ελεγχοι που πραγματοποιουνται:
     * ελεγχος για εισαγωγη αριθμου εκτος επιλογων μενου
     * ελεγχος για εισαγωγη οποιουδηποτε συνδυασμου εκτος αριθμου (int)
     * ελεγχος αν υπαρχει ηδη το ατομο (καθηγητης η φοιτητης)
     */
    private void AddPerson() {

        int option;
        System.out.println("Για προσθήκη καθηγητή πληκτρολογήστε: 1");
        System.out.println("Για προσθήκη φοιτητή πληκτρολογήστε: 2");
        System.out.println("Eπιλογή: ");
        do {
            while (!input.hasNextInt()) {
                System.out.println("Δεν επιλέξατε αριθμό.");
                System.out.println("Δώστε αποδεκτή τιμή επιλογής: ");
                input.next();
            }
            option = input.nextInt();
        } while (option <= 0);
        input.nextLine();

        if (option == 1) {
            Professor p;
            System.out.printf("Eισάγετε κωδικό καθηγητή: ");
            String tmpProfCode = input.nextLine();

            if (MainList.CheckProf(tmpProfCode)) {
                System.out.println("O καθηγητής υπάρχει ήδη.");
                return;
            }
            System.out.printf("Eισάγετε επίθετο: ");
            String tmpSurName = input.nextLine();
            System.out.printf("Eισάγετε όνομα: ");
            String tmpFirstName = input.nextLine();

            System.out.printf("Eισάγετε τηλέφωνο: ");
            String tmpPhone = input.nextLine();

            System.out.printf("Eισάγετε ειδικότητα: ");
            String tmpExpertise = input.nextLine();

            System.out.printf("Eισάγετε Email : ");
            String tmpEmail = input.nextLine();
            p = new Professor(tmpProfCode, tmpSurName, tmpFirstName, tmpEmail, tmpPhone, tmpExpertise);
            MainList.AddPerson(p);
            System.out.println("Eπιτυχής εισαγωγή καθηγητή.");
        } else if (option == 2) {
            Student s;
            System.out.printf("Eισάγετε αριθμό μητρώου φοιτητή: ");
            String tmpAM = input.nextLine();

            if (MainList.CheckStudent(tmpAM)) {
                System.out.println("O φοιτητής υπάρχει ήδη");
                return;
            }

            System.out.printf("Eισάγετε επίθετο: ");
            String tmpSurName = input.nextLine();
            System.out.printf("Eισάγετε όνομα: ");
            String tmpFirstName = input.nextLine();
            System.out.printf("Eισάγετε τηλέφωνο: ");
            String tmpEmail = input.nextLine();
            System.out.printf("Eισάγετε email: ");
            String tmpPhone = input.nextLine();
            System.out.printf("Eισάγετε Eξάμηνο: ");
            int tmpSemester;

            do {
                while (!input.hasNextInt()) {

                    System.out.println("Δεν επιλέξατε αριθμό.");
                    System.out.printf("Δώστε αποδεκτή τιμή εξαμήνου...");
                    input.next();
                }
                tmpSemester = input.nextInt();
            } while (tmpSemester <= 0);

            s = new Student(tmpAM, tmpSurName, tmpFirstName, tmpEmail, tmpPhone, tmpSemester);
            MainList.AddPerson(s);
            System.out.println("Επιτυχής εισαγωγή φοιτητή.");
        } else {
            System.out.println("Δεν υπάρχει αυτή η επιλογή.");

        }
    }


    /**
     * Μεθοδος διορθωσης στοιχειων ατομου στη λιστα των ατομων
     * τυπωνει την εγγραφη μετα τις αλλαγες
     * Ελεγχοι που πραγματοποιουνται:
     * ελεγχος για εισαγωγη οποιοδηποτε αλλου συνδυασμου ψηφιων εκτος αριθμου (int)
     * ελεγχος αν ΔΕΝ υπαρχει το ατομο
     * ζητειται επιβεβαιωση πριν γινει αλλαγη
     */
    private void ManipulatePerson() {
        char option;

        input.nextLine();//clear input buffer

        System.out.printf("Εισάγετε τα στοιχεία του ατόμου προς αλλαγή: \n");
        System.out.printf("Εισάγετε επίθετο: ");
        String tmpSurName = input.nextLine();
        System.out.printf("Eισάγετε όνομα: ");
        String tmpFirstName = input.nextLine();
        Person tmpP = MainList.FindPerson(tmpSurName, tmpFirstName);

        if (!MainList.CheckPerson(tmpP)) {
            return;
        }


        System.out.println("");
        System.out.printf("\" Είστε σίγουροι ότι θέλετε να αλλάξετε τα στοιχεία του ατόμου:\n" + tmpP + "\nY/N?: ");

        do {
            option = input.next().charAt(0);
            input.nextLine();
            if (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν')
                break;
            else if (option == 'Y' || option == 'y' || option == 'υ' || option == 'Υ') {

                System.out.printf("Δώστε το νέο επίθετο:  ");
                String newSurName = input.nextLine();
                tmpP.setSurName(newSurName);
                System.out.printf("Δώστε το νέο όνομα:  ");
                String newFirstName = input.nextLine();
                tmpP.setFirstName(newFirstName);


                System.out.printf("Δώστε το νέο email:  ");
                String newEmail = input.nextLine();
                tmpP.setEmail(newEmail);


                System.out.printf("Δώστε το νέο τηλέφωνο:  ");
                String newPhone = input.nextLine();
                tmpP.setPhone(newPhone);

                if (tmpP instanceof Student) {

                    System.out.printf("Δώστε το νέο αριθμό μητρώου:");
                    String newAM = input.nextLine();
                    ((Student) tmpP).setAM(newAM);
                    System.out.printf("Δώστε το νέο εξάμηνο:");


                    int newSemester;

                    do {
                        while (!input.hasNextInt()) {

                            System.out.println("Δεν επιλέξατε αριθμό!");
                            System.out.printf("Δώστε αποδεκτή τιμή εξαμήνου...");
                            input.next();
                        }
                        newSemester = input.nextInt();
                    } while (newSemester <= 0);


                    ((Student) tmpP).setSemester(newSemester);


                } else if (tmpP instanceof Professor) {

                    System.out.printf("Δώστε το νέο κωδικό καθηγητή: ");
                    String newProfCode = input.nextLine();
                    ((Professor) tmpP).setProfCode(newProfCode);
                    System.out.printf("Δώστε τη νέα ειδικότητα: ");
                    String newExpertise = input.nextLine();
                    ((Professor) tmpP).setExpertise(newExpertise);
                }


            }
            System.out.println("");

            System.out.println("H εγγραφή με τις αλλαγές είναι η :\n" + tmpP);


        } while (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν');


    }

    /**
     * Μεθοδος διαγραφης ατομου απο τη λιστα των ατομων
     * ελεγχοι που πραγματοποιουνται:
     * ελεγχος αν υπαρχει το ατομο
     * ζητειται επιβεβαιωση πριν γινει διαγραφη
     */
    private void DeletePerson() {

        char option;

        input.nextLine();//clear input buffer

        System.out.printf("Eισάγετε τα στοιχεία του ατόμου προς διαγραφή: \n");
        System.out.printf("Εισάγετε επίθετο: ");
        String tmpSurName = input.nextLine();
        System.out.printf("Eισάγετε όνομα: ");
        String tmpFirstName = input.nextLine();
        Person tmpP = MainList.FindPerson(tmpSurName, tmpFirstName);

        if (!MainList.CheckPerson(tmpP)) {
            return;
        }


        System.out.println("");
        System.out.printf("\" ΠΡΟΣΟΧΗ: Eίστε σίγουροι ότι θέλετε να διαγράψετε τα στοιχεία του ατόμου:\n" + tmpP + "\nY/N?: ");

        do {
            option = input.next().charAt(0);

            if (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν')
                break;
            else if (option == 'Y' || option == 'y' || option == 'υ' || option == 'Υ')
                MainList.DelPerson(tmpP);
            System.out.println("Eπιτυχής διαγραφή ατόμου.");
        } while (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν');


    }

    /**
     * Μεθοδος προσθηκης νεου μαθηματος στη λιστα των μαθηματων
     * Ελεγχοι που πραγματοποιουνται:
     * Ελεγχος για εισαγωγη οποιοδηποτε αλλου συνδυασμου ψηφιων εκτος αριθμου (int)
     * ελεγχος αν υπαρχει ηδη το μαθημα
     */
    private void AddNewCourse() {
        input.nextLine();
        Course c;

        System.out.printf("Εισάγετε κωδικό ή λεκτικό μαθήματος: ");
        String tmpCourseID = input.nextLine();

        if (MainList.CheckCourse(tmpCourseID)) {
            System.out.println("To μάθημα υπάρχει ήδη.");
            return;
        }
        System.out.printf("Eισάγετε τίτλο μαθήματος: ");
        String tmpCourseName = input.nextLine();
        System.out.printf("Eισάγετε εξάμηνο μαθήματος: ");
        int tmpCourseSemester;

        do {
            while (!input.hasNextInt()) {

                System.out.println("Δεν επιλέξατε αριθμό!");
                System.out.printf("Δώστε αποδεκτή τιμή εξαμήνου...");
                input.next();
            }
            tmpCourseSemester = input.nextInt();
        } while (tmpCourseSemester <= 0);

        c = new Course(tmpCourseID, tmpCourseName, tmpCourseSemester);
        MainList.AddCourse(c);
        System.out.println("Eπιτυχής εισαγωγή μαθήματος.");

    }

    /**
     * μεθοδος μεταβολης μαθηματος στη λιστα των μαθηματων
     * τυπωνει την εγγραφη μετα τις αλλαγες
     * ελεγχοι που πραγματοποιουνται
     * ελεγχος για εισαγωγη οποιουδηποτε αλλου συνδυασμου οποιων εκτος αριθμου (int)
     * ελεγχος αν ΔΕΝ υπαρχει ηδη το μαθημα
     */
    private void ManipulateCourse() {

        input.nextLine();
        char option;
        System.out.printf("Eισάγετε κωδικό ή λεκτικό μαθήματος προς αλλαγή:");
        String tmpCourseID = input.nextLine();


        Course tmpC = MainList.GetCourseByID(tmpCourseID);


        if (!MainList.CheckCourse(tmpCourseID)) {
            System.out.println("To μάθημα δεν υπάρχει στη λίστα.");
            return;
        }

        System.out.println("");
        System.out.printf("\" Eίστε σίγουροι ότι θέλετε να αλλάξετε το μάθημα:\n" + tmpC + "\nY/N?: ");

        do {
            option = input.next().charAt(0);

            if (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν')
                break;
            else if (option == 'Y' || option == 'y' || option == 'υ' || option == 'Υ') {
                input.nextLine();

                System.out.printf("Εισαγωγή τίτλου μαθήματος: ");
                String tmpCourseName = input.nextLine();
                tmpC.setCourseName(tmpCourseName);
                System.out.printf("Eισαγωγή εξαμήνου μαθήματος: ");
                int tmpCourseSemester;

                do {
                    while (!input.hasNextInt()) {

                        System.out.println("Δεν επιλέξατε αριθμό!");
                        System.out.printf("Δώστε αποδεκτή τιμή εξαμήνου...");
                        input.next();
                    }
                    tmpCourseSemester = input.nextInt();
                } while (tmpCourseSemester <= 0);

                tmpC.setCourseSemester(tmpCourseSemester);


            }
            System.out.println("");
            System.out.println("Η εγγραφή μετά τις αλλαγές είναι η:\n" + tmpC);


        } while (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν');

    }


    /**
     * Μεθοδος διαγραφης μαθηματος απο τη λιστα των μαθηματων
     * Ελεγχοι που πραγματοποιουνται:
     * ελεγχος αν υπαρχει το μαθημα
     * ζητειται επιβεβαιωση πριν γινει διαγραφη
     */
    private void DeleteCourse() {

        input.nextLine();
        char option;

        System.out.printf("Eισάγετε κωδικό ή λεκτικό μαθήματος προς διαγραφή : \n");
        String tmpCourseID = input.nextLine();

        Course tmpC = MainList.GetCourseByID((tmpCourseID));

        if (!MainList.CheckCourse(tmpCourseID)) {
            System.out.println("Δεν υπάρχει τέτοιο μάθημα στη λίστα.");
            return;
        }


        System.out.println("");
        System.out.printf("\" ΠΡΟΣΟΧΗ: Είστε σίγουροι ότι θέλετε να διαγράψετε το μάθημα:\n" + tmpC + "\nY/N?: ");

        do {
            option = input.next().charAt(0);

            if (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν')
                break;
            else if (option == 'Y' || option == 'y' || option == 'υ' || option == 'Υ')
                MainList.DelCourse(tmpC);
            System.out.println("Eπιτυχής διαγραφή μαθήματος.");
        } while (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν');


    }

    /**
     * Μεθοδος αναθεσης μαθηματος σε ατομο
     * Ελεγχοι που πραγματοποιουνται:
     * ελεγχος αν υπαρχει το ατομο
     * ελεγχος αν του εχει ανατεθει ηδη μαθημα
     */
    private void AddCourseToPerson() {

        input.nextLine();
        System.out.printf("Eισάγετε τα στοιχεία του ατόμου για ανάθεση μαθήματος:\n");
        System.out.printf("Εισάγετε επίθετο: ");
        String tmpSurName = input.nextLine();
        System.out.printf("Eισάγετε όνομα: ");
        String tmpFirstName = input.nextLine();

        Person tmpP = MainList.FindPerson(tmpSurName, tmpFirstName);

        if (!MainList.CheckPerson(tmpP)) {
            return;
        }

        System.out.println("");
        System.out.println("Tα στοιχεία του ατόμου στο οποίο θα γίνει η ανάθεση του μαθηματος είναι :\n" + tmpP);
        System.out.println("");


        System.out.printf("Eισάγετε κωδικό ή λεκτικό μαθήματος προς ανάθεση: ");
        String tmpCourseID = input.nextLine();

        Course tmpC = MainList.GetCourseByID(tmpCourseID);

        if (tmpP.HasCourse(tmpC.getCourseID()))
            System.out.println("To μάθημα υπάρχει ήδη.");

        else {
            tmpP.AddCourse(tmpC);
            System.out.println("");
            System.out.println("Eπιτυχής ανάθεση μαθήματος.");
        }
    }

    /**
     * Μεθοδος διαγραφης μαθηματος απο ατομο (φοιτητη ή καθηγητη)
     * εμφανιζει λιστα των μαθηματων του ατομου ωστε να γινει η επιλογη του μαθηματος προς διαγραφη απο τον κωδικο η το λεκτικο του
     * Ελεγχοι που πραγματοποιουνται:
     * ελεγχος αν υπαρχει το ατομο
     * ελεγχος αν του εχει ανατεθει το ατομο
     * ζητειται επιβεβαιωση πριν γινει διαγραφη
     */
    private void DeleteCourseFromPerson() {
        char option;
        input.nextLine();
        System.out.printf("Eισάγετε τα στοιχεία του ατόμου για διαγραφή μαθήματος :\n");
        System.out.printf("Eισάγετε επίθετo: ");
        String tmpSurName = input.nextLine();
        System.out.printf("Eισάγετε όνομα: ");
        String tmpFirstName = input.nextLine();

        Person tmpP = MainList.FindPerson(tmpSurName, tmpFirstName);

        if (!MainList.CheckPerson(tmpP)) {
            return;
        }

        System.out.println("");
        System.out.println("Tα στοιχεία του ατόμου στο οποίο θα γίνει η διαγραφή του μαθήματος είναι :\n" + tmpP);
        System.out.println("");
        System.out.println("Tα μαθήματα τα οποία έχουν ανατεθεί στο άτομο είναι :\n" + tmpP.GetCoursesNames());
        System.out.println("");


        System.out.printf("Eισάγετε κωδικό ή λεκτικό μαθήματος προς διαγραφή: ");
        String tmpCourseID = input.nextLine();

        Course tmpC = MainList.GetCourseByID(tmpCourseID);

        if (!tmpP.HasCourse(tmpC.getCourseID()))
            System.out.println("To μάθημα δεν υπάρχει.");

        else {
            System.out.println("");
            System.out.printf("\" ΠΡΟΣΟΧΗ: Eίστε σίγουροι ότι θέλετε να διαγράψετε το μάθημα:\n" + MainList.GetCourseByID(tmpCourseID) + "\nY/N?: ");


            option = input.next().charAt(0);

            if (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν')
                return;
            else if (option == 'Y' || option == 'y' || option == 'υ' || option == 'Υ')
                tmpP.RemoveCourse(tmpCourseID);
            System.out.println("Eπιτυχής διαγραφή μαθήματος.");
        }
    }

    /**
     * Μεθοδος εισαγωγης βαθμολογιας σε μαθημα που εχει ανατεθει σε φοιτητη
     * εμφανιζει λιστα των μαθηματων του φοιτητη ωστε να γινει η επιλογη του
     * μαθηματος προς βαθμολογηση απο τον κωδικο του ελεγχου που πραγματοποιειται
     */
    private void AddGrades() {

        input.nextLine();
        char option;
        System.out.printf("Εισάγετε τα στοιχεία του φοιτητή προς βαθμολόγηση:\n");
        System.out.printf("Εισάγετε επίθετο: ");
        String tmpSurName = input.nextLine();
        System.out.printf("Eισάγετε όνομα: ");
        String tmpFirstName = input.nextLine();

        Person tmpP = MainList.FindPerson(tmpSurName, tmpFirstName);
        if (!MainList.CheckPerson(tmpP)) {
            return;
        }

        if (tmpP instanceof Student) {
            Student tmpS = (Student) tmpP;
            System.out.println("Tα μαθήματα στα οποία έχει κάνει εγγραφή ο φοιτητής:\n" + tmpP + " είναι:");
            System.out.println(tmpP.getCourses());

            Grade g;
            boolean check = false;
            double tmpGrade;

            do {
                String tmpG_Student_AM = tmpS.getAM();
                System.out.printf("Εισάγετε κωδικό ή λεκτικό μαθήματος προς βαθμολόγηση: ");
                String tmpG_CourseID = input.nextLine();

                if (tmpS.HasCourse(tmpG_CourseID)) {
                    if (MainList.CheckGrade(tmpG_CourseID)){

                    g = MainList.FindGrade(tmpG_CourseID);
                    System.out.println("Yπάρχει ήδη βαθμολογία για το μάθημα" + MainList.GetCourseByID(tmpG_CourseID));
                    System.out.println("H βαθμολογία είναι: " + MainList.FindGrade(tmpG_CourseID));
                    System.out.printf("Θέλετε να αλλάξετε βαθμολογία ? Y/N");

                    option = (char) input.nextInt();

                    if (option == 'N' || option == 'n' || option == 'Ν' || option == 'ν')
                        return;
                    else if (option == 'Y' || option == 'y' || option == 'υ' || option == 'Υ') {
                        System.out.printf("Εισάγετε βαθμολόγηση μαθήματος:");
                        do {
                            while (!input.hasNextDouble()) {
                                System.out.println("Aυτός δεν είναι σωστός βαθμός.");
                                System.out.println("Δώστε αποδεκτή βαθμολογία: ");
                                input.next();
                            }
                            tmpGrade = input.nextDouble();
                        } while (tmpGrade <= 0);
                        g.setGrade(tmpGrade);
                        System.out.println("Eπιτυχής αλλαγή βαθμολογίας." + g);
                        return;
                    }


                } else {
                    System.out.printf("Eισάγετε βαθμολογία μαθήματος:");

                    do {
                        while (!input.hasNextDouble()) {
                            System.out.println("Αυτός δεν είναι ωστός βαθμός");
                            System.out.printf("Δώστε αποδεκτή βαθμολογία:");
                            input.next();
                        }

                        tmpGrade = input.nextDouble();
                    } while (tmpGrade <= 0);

                    try {
                        g = new Grade(tmpG_Student_AM, tmpG_CourseID, tmpGrade);
                        check = true;
                        MainList.AddGrade(g);
                        System.out.println("Επιτυχής εισαγωγή βαθμολογίας. " + g);
                    } 
                    catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
                }
                else 
              System.out.println("auto to mathima den exei anatethei se foititi");
            } while (!check);
        }
        else
            System.out.println("Δεν υπάρχει φοιτητής με αυτά τα στοιχεία.");
    }


    /**
     * Μεθοδος εμφανισης μεσου ορου βαθμολογιας φοιτητη
     * δεν γινεται χρηση της μεθοδου στην υλοποιηση
     * ελεγχοι που πραγματοποιουνται:
     * ελεγχος αν υπαρχει φοιτητης
     */
    public void AvgOfStudent() {

        System.out.printf("Eισάγετε τα στοιχεία του φοιτητή για αναζήτηση : \n");
        System.out.printf("Εισάγετε επίθετο: ");
        String tmpSurName = input.nextLine();
        System.out.printf("Eισάγετε όνομα: ");
        String tmpFirstName = input.nextLine();
        Person tmpP = MainList.FindPerson(tmpSurName, tmpFirstName);

        if (!MainList.CheckPerson(tmpP)) {
            return;
        }

        if (tmpP instanceof Student) {
            Student tmpS = (Student) tmpP;
            System.out.println(" O μέσος όρος του φοιτητή" + tmpS + "\nείναι:" + (MainList.AVG_Student(tmpS.getAM())));

        } else {
            System.out.println("Δεν υπάρχει φοιτητής με αυτά τα στοιχεία.");
        }
    }


    /**
     * Μεθοδος εμφανισης μεσου ορου βαθμολογιας των φοιτητων (ανα φοιτητη)
     * Ελεγχοι που πραγματοποιουνται:
     * ελεγχος αν η λιστα ατομων ειναι κενη
     */
    private void AvgOfAllStudents() {

        if (MainList.getP_list().isEmpty())
            System.out.println("H λίστα ατόμων είναι κενή.");
        else {
            System.out.println("Μέσος όρος βαθμολογιών ανά φοιτητή");
            System.out.println("====================================");
            System.out.println("AM      Eπίθετo           Όνομα          M/O Mαθημάτων");
            System.out.println("-----------------------------------------------");

            for (Person S : MainList.getP_list()) {
                if (S instanceof Student) {

                    Student tmpS = (Student) S;
                    System.out.printf("%-6s  %-15s  %-15s %f\n", tmpS.getAM(), tmpS.getSurName(), tmpS.getFirstName(),
                            MainList.AVG_Student(tmpS.getAM()));
                }
            }
        }
        System.out.println("");
    }

    /**
     * Μεθοδος εμφανισης μεσου ορου βαΘμολογιας ολων των μαθηματων ανα μαθημα
     * ελεγχος που πραγματοποιειται:
     * αν η λιστα μαθηματων ειναι κενη
     */
    private void AvgOfAllCourses() {

        if (MainList.getP_list().isEmpty()) {
            System.out.println("H λίστα μαθημάτων είναι κενή");
        } else {
            System.out.println("Μέσος όρος βαθμολογιών ανά μάθημα ");
            System.out.println("====================================");
            System.out.println("Μάθημα                      M/O ");
            System.out.println("-----------------------------------------------");

            for (Course c : MainList.getC_list()) {

                System.out.printf("%-25s %f\n", c.getCourseName(), MainList.AVG_Course(c.getCourseID()));
            }
        }
        System.out.println("");
    }

    /**
     * Μεθοδος αποθηκευσης των δεδομενων ατομων (καθηγητων και φοιτητων), μαθηματων και βαθμολογιων σε αντιστοιχα αρχεια
     * Οι λιστες καθηγητων, φοιτητων, μαθηματων και βαθμολογιων εξαγονται στην επιφανεια εργασιας αφου ζητηθει το ονομα του προς αποθηκευση αρχειου
     * Εχουμε την εξαγωγη των αρχειων
     * Ελεγχοι που πραγματοποιουνται:
     * γινεται ελεγχος με exception σχετικα με την αναγνωση/αποθηκευση των αρχειων
     */
    private void SaveData() {

        String FnC;
        boolean Rc;

        String FnP;
        boolean Rp;

        String FnS;
        boolean Rs;

        String FnG;
        boolean Rg;

        System.out.println("  Αποθήκευση στοιχείων μαθημάτων");
        System.out.println("Όνομα του προς αναζήτηση αρχείου λίστας μαθημάτων");
        FnC = input.nextLine();
        Rc = MainList.SaveCourses(FnC);
        if (Rc) {
            System.out.println("H αποθήκευση ολοκληρώθηκε...");
        } else {
            System.out.println("H αποθήκευση δεν ολοκληρώθηκε...");
        }

        System.out.println("Αποθήκευση στοιχείων καθηγητών");
        System.out.println("Όνομα του προς αναζήτηση αρχείου λίστας καθηγητών");
        FnP = input.nextLine();
        Rp = MainList.SaveProfessors(FnP);
        if (Rp) {
            System.out.println("H αποθήκευση ολοκληρώθηκε...");
        } else {
            System.out.println("H αποθήκευση δεν ολοκληρώθηκε...");
        }

        System.out.println("  Αποθήκευση στοιχείων φοιτητών");
        System.out.println("Όνομα του προς αναζήτηση αρχείου λίστας φοιτητών");
        FnS = input.nextLine();
        Rs = MainList.SaveStudents(FnS);
        if (Rs) {
            System.out.println("H αποθήκευση ολοκληρώθηκε...");
        } else {
            System.out.println("H αποθήκευση δεν ολοκληρώθηκε...");
        }

        System.out.println("  Αποθήκευση στοιχείων βαθμολογιών");
        System.out.println("Όνομα του προς αναζήτηση αρχείου λίστας βαθμολογιών");
        FnG = input.nextLine();
        Rg = MainList.SaveGrades(FnG);
        if (Rg) {
            System.out.println("H αποθήκευση ολοκληρώθηκε...");
        } else {
            System.out.println("H αποθήκευση δεν ολοκληρώθηκε...");
        }
    }

    /**
     *
     */
    public void LoadData()    {
            String FnC;
        boolean Rc;
        System.out.println("Ανάκτηση στοιχείων μαθημάτων");
        System.out.println("Όνομα του προς ανάκτηση αρχείου λίστας μαθημάτων");
        FnC = input.nextLine();
        Rc = MainList.LoadCourses(FnC);
        if (Rc)
            System.out.println("H Ανάκτηση ολοκληρώθηκε...");
        else
            System.out.println("H Ανάκτηση δεν ολοκληρώθηκε...");
		
		
		String FnP;
		boolean Rp;
		     System.out.println("Ανάκτηση στοιχείων μαθημάτων");
        System.out.println("Όνομα του προς ανάκτηση αρχείου λίστας καθηγητών");
        FnP = input.nextLine();
        Rp = MainList.LoadProfessors(FnP);
        if (Rp)
            System.out.println("H Ανάκτηση ολοκληρώθηκε...");
        else
            System.out.println("H Ανάκτηση δεν ολοκληρώθηκε...");
		
			
		String FnG;
		boolean Rg;
		     System.out.println("Ανάκτηση στοιχείων μαθημάτων");
        System.out.println("Όνομα του προς ανάκτηση αρχείου λίστας  βαΘμολογιας");
        FnG = input.nextLine();
        Rg = MainList.LoadGrades(FnG);
        if (Rg)
            System.out.println("H Ανάκτηση ολοκληρώθηκε...");
        else
            System.out.println("H Ανάκτηση δεν ολοκληρώθηκε...");
		
		
			
		String FnS;
		boolean Rs;
		     System.out.println("Ανάκτηση στοιχείων μαθημάτων");
        System.out.println("Όνομα του προς ανάκτηση αρχείου λίστας φοιτητώνών");
        FnS = input.nextLine();
        Rs = MainList.LoadStudents(FnS);
        if (Rs)
            System.out.println("H Ανάκτηση ολοκληρώθηκε...");
        else
            System.out.println("H Ανάκτηση δεν ολοκληρώθηκε...");
    }


    /**
     * Μενου επιλογης αποθηκευσης ή φορτωσης των δεδομενων της εφαρμογης
     * Ελεγχοι που πραγματοποιουνται:
     * ελεγχος για εισαγωγη μαθηματος
     * ελεγχος για εισαγωγη οποιουδηποτε αλλου συνδυασμου ψηφιων εκτος αριθμου (int)
     */
    private void LoadSaveDialogue()
    {
        int option;
        char choice;
        System.out.println("  Για αποθήκευση των στοιχείων σε αρχείο πατήστε 1");
        System.out.println("  Για ανάγνωση των στοιχείων από αρχείο πατήστε 2");
        System.out.println("  Eπιλογή:");
        do {
            while (!input.hasNextInt()) {
                System.out.println("Δεν επιλέξατε αριθμό ");
                System.out.println("Δώστε αποδεκτή τιμή:");
                input.next();
            }
            option = input.nextInt();
        } while (option <= 0);
        input.nextLine();

        if (option == 1) {
            SaveData();
        } else if (option == 2) {
            LoadData();
        } else {
            System.out.println("Λάθος επιλογή!");
        }
    }
}
