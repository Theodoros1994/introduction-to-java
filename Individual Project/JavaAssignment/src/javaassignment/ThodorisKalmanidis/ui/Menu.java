/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *  Εισαγωγή των βιβλιοθηκών που θα χρησιμοποιηθούν για την υλοποίηση
 */
package javaassignment.ThodorisKalmanidis.ui;
import javaassignment.ThodorisKalmanidi.data.Employee;
import javaassignment.ThodorisKalmanidi.data.EmployeeSortBySalaryAsc;
import javaassignment.ThodorisKalmanidi.data.EmployeeSortBySalaryDesc;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
/**
 *
 * @author teo
 */

/**
 * Ονομασία της μεταβλητής της Λιστάς AllEmployees για την αποθηκευσή των αντικειμένων των υπαλλήλων
 * 
 * Επίσης έχουμε ονομασία της μεταβλήτης keyb που θα χρησιμοποιηθεί για την αναγνωσή από το πληκτρολόγιο
 * @author teo
 */
public class Menu {
 ArrayList<Employee> AllEmployees;
 Scanner Keyb; 
 
 
 /**
  * Ο κατασκευαστής Menu δηλώνει τις μεταβλητές AllEmployees και Keyb που δηλώθηκαν σε public επίπεδο.
  * Συγκεκριένα η AllEmployees δηλώνετε ως ενάς δυναμικός πίνακας τύπου arraylist και η μεταβλητή keyb
  * ως ένα αντικείμενο τύπου scanner οπού θα διαβάζει από το πληκτρολόγιο του συστήματος 
  */
 Menu()   {
        AllEmployees = new ArrayList ();
        Keyb = new Scanner (System.in);
        
    }
  /**
   * Η μεθοδός αυτή χρησιμοποίται για την αρχικοποίηση και αποθήκευση τριών υπαλλήλων κατά την εναρξή του προγράμματος
   * Μετά τη δήλωση των τριών αντικειμένων των υπαλλήλων, προσθέτουμε τους υπάλληλους στη λιστα ArrayList
   * 
   */
     void EmployeeLoad ()
    {
        Employee E1,E2,E3;
        E1 = new Employee (1, "George Drakakis","6931513816",1200.50,"gdrakakis@gmail.com","123456","654321");
        E2 = new Employee (2, "Andreas Zaras","6931513817",810.50,"azaras@gmail.com","223456","654322");
        E3 = new Employee (3, "Dimitrios Vogiatzis","6931513818",1000,"dvogiatzis@gmailcom","323456","654323");
        AllEmployees.add (E1);
        AllEmployees.add (E2);
        AllEmployees.add (E3);
    }
 
     
   /**
    * Η μεθοδός αυτή ψάχνει στη λίστα με τους υπάλληλους ένα ονομά που έχει δώσει ο χρήστης
    * @param EmployeeName Ονομά Υπαλλήλου
    * @return Επιστρέφει το αντικείμενο με τα στοιχεία του Υπαλλήλου αν ύπαχει ή null αν δεν υπάρχει
    */
    Employee EmployeeSearchByName (String EmployeeName )
    {
     
        for (Employee E: AllEmployees)
             if (E.getName().toUpperCase ().startsWith (EmployeeName.toUpperCase ())) // Μετατροπή των ονομάτων της λίστας και αυτού προς ευρεσή σε κεφαλαία
                return E;
        return null;
    }
    /**
     * Η μεθοδός αυτή ψάχνει στη λίστα με τους υπάλληλους ένα τηλέφωνο που έχει δώσει ο χρήστης
     * @param EmployeePhone Τηλέφωνο Υπαλλήλου
     * @return Επιστρέφει το αντικείμενο με τα στοιχεία του Υπαλλήλου αν ύπαχει ή null αν δεν υπάρχει
     */
        Employee EmployeeSearchByPhone (String EmployeePhone)
    {
         for (Employee E: AllEmployees)
             if (E.getPhone().toUpperCase ().startsWith (EmployeePhone.toUpperCase ()))
                return E;
   
        return null;       
    }
     
      /**
       * Η μεθόδος καλέί τον χρήστη νά εισάγει το ονομά του υπάλληλου του οποίου επιθυμεί τη
       * διαγραφή. Στη συνέχεια αν το ονομά υπάρχει το αντικείμενο του υπαλλήλου διαγραφείτε από τη λίστα, διαφορετικά εκτυπώνετε μηνυμά
       * λαθός ονόματος
       */  
    void Delete(){
   
        String Nam;
        Employee E;
     System.out.print ("Εισάγετε ονομά Υπαλλήλου προς διαγραφή....: ");
        //Cod = Keyb.nextInt();
        Nam=Keyb.nextLine();
         if (EmployeeSearchByName (Nam) != null)
             EmployeeDeletion(Nam);
         else{
          System.out.print ("Εισάχθηκε λάθος ονομά Υπαλλήλου....: ");
         }
             
    }

    
        
    // to be checked
    /**
     * Η μεθοδός αυτή δεχεταί σαν όρισμα το ονόμα του υπαλλήλου το οποίο έχει εισάγει ο χρήστης
     * και αν το ονομά του υπαλλήλου βρεθεί ο υπάλληλος διαγράφετε, διαφορετικά εκτυπώνετε ενημερωτικό μηνυμά
     * @param EmployeeName  Ονομά υπάλληλου
     */
      void EmployeeDeletion (String EmployeeName)
    {
        System.out.println ("Εκτελουνται εντολες διαγραφης");


      
          int i;
        Employee P;
        for (i = 0 ; i < AllEmployees.size (); i++)
        {
            P = AllEmployees.get (i);
            if (P.getName().toUpperCase ().startsWith (EmployeeName.toUpperCase ())) {
                  AllEmployees.remove (i); 
                          System.out.println ("Ο υπάλληλος διαγράφηκε....");
            } 
        }
      
     
    }
      /**
       * Η μέθοδος αυτή χρησιμοποίειται για την παύση του προγράμματος ενδιαμέσα σε κάποιες λειτουργίες
       * με σκοπό ο χρήστης να πληκτρολογήσει ENTER ωστέ να συνεχιστεί η εκτελέση του προγράμματος 
       */
      void Pause ()
    {
        System.out.print ("\n\nΠιέστε <Enter> για συνέχεια...");
        Keyb.nextLine ();
        System.out.println ("\n\n");
    } 
      /**
       * Η μεθοδός αυτή προσθέτει καινούργιους υπαλλήλους στη λίστα των υπαλλήλων, ελέγχοντας πώς δεν υπάρχει άλλος υπάλληλος με το ίδιο΄όνομα
       */
     void EmployeeInsert ()
    {
        int Cod;
        String Nam;
        String Phone;
        String mail;
        Double money;
        String aamka;
        String aafm;  
        Employee Ε ;
     //   Employee E1,E2,E3;
        System.out.println ("Εισαγωγή νέου Υπαλλήλου");
        System.out.println ("=======================\n");
        System.out.print ("Εισάγετε κωδικό Υπαλλήλου....: ");
        Cod = Keyb.nextInt();
        Keyb.nextLine();
       
        System.out.print("Εισάγετε όνομα Υπαλλήλου....: ");
        Nam = Keyb.nextLine();
        if (!Nam.matches("[a-zA-Z_]+")) {
    System.out.println("Παρακαλώ εισάγετε χαρακτήρες");
}
        
        System.out.print("Εισάγετε Τηλέφωνο Υπαλλήλου....: ");
        Phone = Keyb.nextLine();
        //Keyb.nextLine();
        if (!Phone.matches("[0-9]+")) {
    System.out.println("Παρακαλώ εισάγετε ψηφία");
}
        
        System.out.print("Εισάγετε Email Υπαλλήλου....: ");
        mail = Keyb.nextLine();
        //Keyb.nextLine();
                if (!mail.matches("[a-zA-Z_]+")) {
    System.out.println("Παρακαλώ εισάγετε χαρακτήρες");
}
        
        System.out.print("Εισάγετε Μισθό Υπαλλήλου....: ");
        money = Keyb.nextDouble();
        Keyb.nextLine();
        
         System.out.print("Εισάγετε ΑΦΜ Υπαλλήλου....: ");
        aafm = Keyb.nextLine();
        if (!aafm.matches("[0-9]+")) {
    System.out.println("Παρακαλώ εισάγετε ψηφία");
}
        
        System.out.print("Εισάγετε ΑΜΚΑ Υπαλλήλου....: ");
        aamka = Keyb.nextLine();
       // Keyb.nextLine();
       if (!aamka.matches("[0-9]+")) {
    System.out.println("Παρακαλώ εισάγετε ψηφία");
}
        //Keyb.nextLine();
        if (EmployeeSearchByName (Nam) != null)
                {
            System.out.println ("Ο Υπάλληλος υπάρχει ήδη");
        }
         
                 
        else   if     (!Nam.matches("[a-zA-Z_]+")) {
    System.out.println("Παρακαλώ εισάγετε σωστό τύπου στοιχείων");
}
        
        
                 
        else   if      (!Phone.matches("[0-9]+")){
    System.out.println("Παρακαλώ εισάγετε σωστό τύπου στοιχείων");
}
                     
        else   if (!mail.matches("[a-zA-Z_]+")){
     System.out.println("Παρακαλώ εισάγετε σωστό τύπου στοιχείων");
}
                       
        else    if (!aafm.matches("[0-9]+")){
    System.out.println("Παρακαλώ εισάγετε σωστό τύπου στοιχείων");
}
                       
  
        
        else         if (!aafm.matches("[0-9]+")) {
      System.out.println("Παρακαλώ εισάγετε σωστό τύπου στοιχείων");
}
           
        
        else
        {   //Employee E1;
            Ε = new Employee (Cod, Nam,Phone,money,mail,aamka,aafm);
            AllEmployees.add (Ε); 
            System.out.println ("Η εισαγωγή Υπαλλήλου ολοκληρώθηκε...");
        }
        Pause ();
        
    }
     /**
      * Η μεθόδος αυτή χρησιμοποιείται για την παρουσίαση όλων των υπαλλήλων, παρουσιάζονται και το συνολικό
      * αριθμό των υπαλλήλων που είναι καταχωρημένη
      */
         void EmployeePresentation ()
    {
        int i;
        i = 1;
        System.out.println ("Παρουσίαση λίστας υπαλλήλων");
        System.out.println ("===========================\n");
        System.out.println ("Αριθμος Υπαλλήλων : " + AllEmployees.size ());
        for (Employee tmp: AllEmployees)
            System.out.printf ("[%2d] %s\n", i++, tmp.toString ());
        Pause ();
    }
     
     
     /**
      * Η μεθοδός αυτή καλείται από το μενού με σκοπό την τυπώση του ενημερωτικού μηνύματος
      * της ταξινόμησης και επειτά της κλησης της συναρτισής ταξινομησής ως προς το μισθό σε αυξούσα σειρά
      */
      void EmployeeSortBySalaryAsc ()
    {
       System.out.println("\n----Αυξουσά Ταξινόμηση Μισθού----");
        Collections.sort(AllEmployees, new EmployeeSortBySalaryAsc());
    } 
     
       /**
       * Η μεθοδός αυτή καλείται από το μενού με σκοπό την τυπώση του ενημερωτικού μηνύματος
      * της ταξινόμησης και επειτά της κλησης της συναρτισής ταξινομησής ως προς το μισθό σε φθίνουσα σειρά
        */  
      void EmployeeSortBySalaryDesc ()
    {
       System.out.println("\n----Φθινουσά Ταξινόμηση Μισθού----");
        Collections.sort(AllEmployees, new EmployeeSortBySalaryDesc());
    } 
     /**
      * Η μεθοδός αυτή καλεί τον χρήστη να πληκτρολογήσει το τηλέφωνο του υπαλλήλου προς ανάζητηση και αν βρεθεί ο υπάλληλος
      * με τον αριθμό αυτόν τοτέ παρουσιάζονται όλα τα στοιχεία αυτού, διαφορετικά εκτυπώνετε μήνυμα μη ευρεσής αριθμού
      */
    void EmployeSearchByPhone(){
       Employee E;
        System.out.println (" Εμφάνιση Καρτέλας Υπαλλήλου");
        System.out.println ("===========================\n");
        System.out.print ("Εισάγετε τον αριθμό του Υπαλλήλου : ");
        String num = Keyb.nextLine();
        E=EmployeeSearchByPhone(num);
        if (E==null){
            System.out.println("Δεν υπάρχει Υπάλληλος με αριθμό " + num);
            Pause(); }
        else{
        
                  System.out.println("Στοιχεία καρτελάς Υπαλλήλου : ");
                   E.ShowEmployeeData();
                      Pause();
        
        }
    
    }
    
    /**
     *       * Η μεθοδός αυτή καλεί τον χρήστη να πληκτρολογήσει το όνομα του υπαλλήλου προς ανάζητηση και αν βρεθεί ο υπάλληλος
      * με το ονομά αυτό τοτέ παρουσιάζονται όλα τα στοιχεία αυτού, διαφορετικά εκτυπώνετε μήνυμα μη ευρεσής ονόματος 
     */
    void EmployeSearchByName()
    
    {
        Employee E;
        System.out.println ("Εμφάνιση Καρτέλας Υπαλλήλου");
        System.out.println ("===========================\n");
        System.out.print ("Εισάγετε το ονομά του Υπαλλήλου : ");
        String name = Keyb.nextLine();
        E=EmployeeSearchByName(name);
          if (E==null){
          System.out.println("Δεν υπάρχει Υπάλληλος με Ονομά " + name);
          Pause();
          
          }
        else{
        
                  System.out.println("Στοιχεία καρτελάς Υπαλλήλου : ");
                   E.ShowEmployeeData();
                      Pause();
        
        }
    }
      
     /**
      * Η μεθοδός αυτή χρησιμοποιείται για την διορθώση των στοιχείων του υπαλλήλου
      */
       void EmployeeEdit ()
    {
        String TbS;
        Employee Ε;
        int TmpInt;
        Double TmpDouble;
        String TmpStr;
        System.out.println ("Διόρθωση Καρτέλας Υπαλλήλου");
        System.out.println ("============================\n");
        System.out.print ("Εισάγετε το ονομά του υπαλλήλου : ");
        TbS = Keyb.nextLine ();
        Ε = EmployeeSearchByName (TbS);
        if (Ε == null)
        {
            System.out.println ("Δεν υπάρχει υπάλληλος με ονομά " + TbS);
            return;
        }
        System.out.print ("Κωδικός Υπαλλήλου..... : " + Ε.getEmployeeID () + "  : ");
        TmpInt = Keyb.nextInt ();
        Keyb.nextLine ();
        if (TmpInt != 0)
            Ε.setEmployeeID (TmpInt);
        System.out.print ("Ονομά Υπαλλήλου... : " + Ε.getName () + "  : ");
        TmpStr = Keyb.nextLine ();
        if (!TmpStr.equals (""))
            Ε.setName (TmpStr);
        
        
        
        
         System.out.print ("Τηλέφωνο Υπαλλήλου... : " + Ε.getPhone () + "  : ");
        TmpStr = Keyb.nextLine ();
        if (!TmpStr.equals (""))
            Ε.setPhone (TmpStr);
        
        
         System.out.print ("Email Υπαλλήλου... : " + Ε.getEmail () + "  : ");
        TmpStr = Keyb.nextLine ();
        //Keyb.nextLine ();
        if (!TmpStr.equals (""))
            Ε.setEmail (TmpStr);
        
        
        
         System.out.print ("Μισθός Υπαλλήλου... : " + Ε.getSalary () + "  : ");
        TmpDouble = Keyb.nextDouble ();
        if (TmpDouble !=0.0)
            Ε.setSalary (TmpDouble);
        
        
         Keyb.nextLine ();
         System.out.print ("ΑΜΚΑ Υπαλλήλου... : " + Ε.getAmka () + "  : ");
        TmpStr = Keyb.nextLine ();
        if (!TmpStr.equals (""))
            Ε.setAmka (TmpStr);
        
        
         System.out.print ("ΑΦΜ Υπαλλήλου ... : " + Ε.getAfm () + "  : ");
        TmpStr = Keyb.nextLine ();
        if (!TmpStr.equals (""))
            Ε.setAfm (TmpStr);
        
        
        
        System.out.println("Η διορθωσή στοιχείων ολοκληρώθηκε");  
        Pause ();
        
    } 
       /**
        * 
        */
        void Menu ()
    {
        int ch;
        do
        {
            //Καθαρισμος οθονης
            System.out.println ("     Employee System    ");
            System.out.println ("[1]... Εισαγωγή νέου υπαλλήλου");
            System.out.println ("[2]... Παρουσιαση όλων των υπαλλήλων");
            //System.out.println ("[3]... Καρτέλα Υπαλλήλου");
                     
            System.out.println ("[3]... Αναζήτηση Υπαλληλού με Τηλεφωνικό Αριθμό");
            System.out.println ("[4]... Αναζήτηση Υπαλληλού με Ονομά");
            System.out.println ("[5]... Διόρθωση στοιχείων");
            System.out.println ("[6]... Ταξινόμηση με βάση τον μισθό αύξουσα");
            System.out.println ("[7]... Ταξινόμηση με βάση τον μισθό φθίνουσα");
                 
            System.out.println ("[8]... Διαγραφή Υπαλλήλου");
            System.out.println ("[9]... Εξοδος");
            System.out.print ("Επιλογη : ");
            ch = Keyb.nextInt ();
            Keyb.nextLine ();
            switch (ch)
            {
                case 1: EmployeeInsert (); break;
                case 2: EmployeePresentation (); break;
                case 5: EmployeeEdit (); break;
                case 4: EmployeSearchByName (); break;
                case 3: EmployeSearchByPhone (); break;
                case 6: EmployeeSortBySalaryAsc (); break;
                case 7: EmployeeSortBySalaryDesc (); break;
                //case 8: EmployeeEdit (); break;
                case 8: Delete (); break;
            }
        }
        while (ch != 9);
        System.out.println ("Πατηθηκε Εξοδος");
    }  
     
      
 /**
  *  Η μεθοδός αυτή είναι η πρωτή που εκτελείται κατά την εκίνηση του προγράμματος,
  * καλόντας την δημιουργία ενός αντικειμένου της κλάσης μενού Μ το οποίο χρησιμοποιείται
  * για την κλήση των μεθόδων karfota() οπού προστίθεται οι πρώτοι 3 υπάλληλοι στο προγραμμά και την  Menu()
  * μεθοδό οπού εκτυπώνει τις λειτουργίες του μενού στον χρηστή.
  * @param args 
  */
    public static void main(String[] args) {
        // TODO code application logic here
        Menu M = new Menu ();
        M.EmployeeLoad ();
        M.Menu ();
        
    }
    
}
