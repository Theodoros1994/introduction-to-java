/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaassignment.ThodorisKalmanidi.data;



/**
 * H κλάση Employee αναπαρηστά ενάν υπάλληλο. Εμπεριέχει ολές τις πληροφορίες ενός υπαλλήλοτ
 * @author teo
 */


public class Employee {
   /**
    * Δηλώση του τύπου μεταβλητής των στοιχείων της κλάσης υπαλλήλου
    * Ο κωδικός υπάλληλου είναι ακέραιος αριθμός ενώ ο ο μισθός είναι δεκαδικός
    * Τα υπόλοιπα στοιχεία ειναί σε μορφή αλφαριθμητικού
    */
    
 private int EmployeeID;
    private String Name;
    private String Phone;
    private String Email;
    private double Salary;
    private String Amka;
    private String Afm;
    
    
    
    // * Δηλώση του getter του κωδικού υπαλλήλου getters & setters των στοιχείων της κλάσης υπαλλήλος με σκοπό
    //την προσθήκη και λήψη πληροφορίας στη κλάση υπάλληλος
    
    
    /**
     * Δηλώση του getter του κωδικού υπαλλήλου 
     * @return Κωδικός υπαλλήλου
     */
    public int getEmployeeID() {
        return EmployeeID;
    }

    /**
     * Δηλώση του setter του κωδικού υπαλλήλου
     * @param EmployeeID Κωδικός υπαλλήλου
     */
    public void setEmployeeID(int EmployeeID) {
        this.EmployeeID = EmployeeID;
    }

    /**
     * Δηλώση του getter του ονόματος υπαλλήλου
     * @return Ονομά υπάλληλου
     */
    public String getName() {
        return Name;
    }
    /**
     * Δηλωσή του setter του ονόματος υπαλλήλου
     * @param Name  Ονομά υπάλληλου
     */

    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * Δηλώση του getter του τηλεφωνικού αριθμού του υπαλλήλου
     * @return Τηλεφωνικός αριθμός υπαλλήλου
     */
    public String getPhone() {
        return Phone;
    }
    
    /**
     * Δηλώση του setter του τηλεφωνικού αριθμού του υπαλλήλου
     * @param Phone Τηλεφωνικός αριθμός υπαλλήλου
     */

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }
/**
 * Δηλώση του getter του Email υπαλλήλου
 * @return  Email υπαλλήλου
 */
    public String getEmail() {
        return Email;
    }
    
/**
 * Δηλώση του setter του Email του υπαλλήλου
 * @param Email Email υπαλλήλου
 */
    public void setEmail(String Email) {
        this.Email = Email;
    }
    /**
     * Δηλώση του getter του μισθού υπαλλήλου
     * @return Μισθός υπαλλήλος
     */

    public double getSalary() {
        return Salary;
    }
    /**
     * Δηλωσή του setter μισθού του υπαλλήλου
     * @param Salary Μισθός υπαλλήλου
     */

    public void setSalary(double Salary) {
        this.Salary = Salary;
    }

    /**
     * Δηλωσή του getter του ΑΜΚΑ
     * @return ΑΜΚΑ υπαλλήλου
     */
    public String getAmka() {
        return Amka;
    }
    /**
     * Δηλωσή του setter του ΑΜΚΑ του υπαλλήλου 
     * @param Amka ΑΜΚΑ υπαλλήλου
     */

    public void setAmka(String Amka) {
        this.Amka = Amka;
    }
    
    /**
     * Δηλωσή του getter του ΑΦΜ του υπαλλήλου
     * @return ΑΦΜ υπαλλήλου
     */

    public String getAfm() {
        return Afm;
    }
/**
 * Δηλωσή του setter του ΑΦΜ του υπαλλήλου
 * @param Afm ΑΦΜ υπαλλήλου 
 */
    public void setAfm(String Afm) {
        this.Afm = Afm;
    }
 
   
    
    /**
     *  Ο constructor Employee κατασκευάζει το αντικείμενο αρχικοποιώντας όλα τα χαρακτηριστικά
     * @param ID Κωδικός Υπαλλήλου
     * @param N Ονομά Υπαλλήλου
     * @param P Τηλέφωνο Υπαλλήλου
     * @param S Μισθός Υπαλλήλου
     * @param E Email Υπαλλήλου
     * @param Amk ΑΜΚΑ Υπαλλήλου
     * @param Af  ΑΦΜ Υπαλλήλου 
     */
    public Employee (int ID,String N, String P, double S,String E,String Amk, String Af){
    
        EmployeeID=ID;
        Name=N;
        Phone=P;
        Email = E;
        Salary = S;
        Amka = Amk;
        Afm = Af;


    
    
    
    }
    
    /**
     * Η μεθόδος αυτή μας παρουσιάζει ολά τα στοιχεία ολών των υπαλλήλων
     */
    public void ShowEmployeeData(){
    
     System.out.println ("Κωδικός Υπαλλήλου ...........: " + EmployeeID);
     System.out.println ("Ονομα Υπαλλήλου ........ : " + Name);
          System.out.println ("Τηλέφωνο Υπαλλήλου ...........: " + Phone);
        System.out.println ("Email Υπαλλήλου ........ : " + Email);
         System.out.println ("Μισθός Υπαλλήλου ...........: " + Salary);
        System.out.println ("ΑΜΚΑ Υπαλλήλου ........ : " + Amka);
         System.out.println ("ΑΦΜ Υπαλλήλου ...........: " + Afm);
        
        
    
    
    
    
    
    }
    
    
    
    
    
    
    
    
    
    
    
    
   
    
    /**
     * Η μεθοδός αυτή συγκεντρώνει όλα τα στοιχεία ενός υπάλληλου, οπού μπορεί να 
     * βρισκόνται σε διαφορές μορφές όπως αλφαριθμητικό ή ακεραίος αριθμός σε μια γραμμή
     * Επικαλύπτει την αρχική συνάρτιση toString() που μας έδωσε ο κατασκευαστής
     * @return Ολά τα στοιχεία ενός υπαλλήλου σε μορφή String
     */
    
      @Override
    public String toString ()
    {
        String R = EmployeeID + " - " + Name + " - " + Phone + " - " + Email 
                + " - " + Salary + " - " + Amka + " - " + Afm;
        return R;
    }
    
    
    
     /**
     * Η μεθόδος αυτή μας παρουσιάζει ολά τα στοιχεία του υπαλλήλου για τον οποίο
     * εχεί καλεστεί η μεθοδός αυτή
     */
    public void ShowData ()
    {
        System.out.println ("Κωδικός  Υπαλλήλου...........: " + EmployeeID);
        System.out.println ("Ονομά    Υπαλλήλου ..........: " + Name);
        System.out.println ("Τηλέφωνο Υπαλλήλου ..........: " + Phone);
        System.out.println ("Μισθός    Υπαλλήλου..........: " + Salary);
        System.out.println ("Email    Υπαλλήλου ..........: " + Email);
        System.out.println ("ΑΜΚΑ     Υπαλλήλου ..........: " + Amka);
        System.out.println ("ΑΦΜ      Υπαλλήλου ..........: " + Afm);
                    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
