/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaassignment.ThodorisKalmanidi.data;
import java.util.Comparator;
/**
 * Η κλάση αυτή χρησιμοποίειτα ως επέκταση της κλάσης Comparator του κατασκευάστη
 * με σκοπό τη συγκρισή των αντικειμένων των υπαλλήλών ως προς το μισθό σε φθίνουσα σειρά
 * @author teo
 */
 public class EmployeeSortBySalaryDesc implements Comparator <Employee>{
  
  /**
   * Η μεθόδος αυτή συγκρίνει τα αντικείμενα μια συνολίκης συλλόγης αντικειμένων.
   * Καθέ υπάλληλος προσδιορίζεται από ένα αντικείμενο τυπού Employee και επειτά προστίθεται σε 
   * μια λίστα. H μεθοδός αυτή συγκρίνει κάθε αντικείμενο που εμπεριέχεται στη λίστα αυτή με βάση το μισθό
   * και επιστρέφει 3 τιμές ανάλογα το αποτέλεσμα της συγκρισής. Σε περίπτωση ισότητας επιστρέφει το 0. Σε περίπτωση οπού το πρώτο
   * αντικείμενο έχει μεγαλύτερο μισθό από το δεύτερο επιστρέφει το -1, διαφορετικά το 1
   * @param emp1 Πρωτό αντικείμενο υπάλληλου συγκρίσης
   * @param emp2 Δευτερό αντικείμενο υπάλληλου συγκρίσης
   * @return Επιστρέφει μια λίστα αντικειμένων ταξινομημένη σε φθίνουσα σειρά ως προς το μισθό
   */   
  @Override
  public int compare(Employee emp1, Employee emp2) {
        int value = 0;
        if (emp1.getSalary() < emp2.getSalary())
            value = 1;
        else if (emp1.getSalary() > emp2.getSalary())
            value = -1;
        else if (emp1.getSalary() == emp2.getSalary())
            value = 0;
 
        return value;
    }
    
}